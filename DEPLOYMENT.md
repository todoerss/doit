# Deploying Doit app to Kubernetes

## Prerequisites

- Kubectl installed on local machine
- Helm installed on local machine
- Running Kubernetes cluster
- CertManager to issue Let's Encrypt certificates (with `letsencrypt-prod` configuration)
- Running PostgreSQL database (it doesn't have to be running in Kubernetes)

## Preparing environment

### Stage

Create a namespace for the app:

```shell
kubectl create namespace doit
```

Create secret specifying database credentials:

```shell
kubectl create secret generic -n doit db \
  --from-literal=host=<DATABASE HOST> \
  --from-literal=port=<DATABASE PORT> \
  --from-literal=user=<DATABASE USER> \
  --from-literal=password=<DATABASE PASSWORD>
```

k### Prod

Create a namespace for the app:

```shell
kubectl create namespace doit-prod
```

Create secret specifying database credentials:

```shell
kubectl create secret generic -n doit-prod db \
  --from-literal=host=<DATABASE HOST> \
  --from-literal=port=<DATABASE PORT> \
  --from-literal=user=<DATABASE USER> \
  --from-literal=password=<DATABASE PASSWORD>
```

## Deployment & upgrade

### Stage

For stage deployment, execute the following from the project directory:

```shell
helm upgrade --install -n doit doit -f doit-chart/values-stage.yaml doit-chart
```

Or, if chart did not change, simply restart the deployment to re-pull latest image:

```shell
kubectl rollout restart -n doit deployment doit-doit-chart 
```


### Prod

For prod deployment, execute the following from the project directory:

```shell
helm upgrade --install -n doit-prod doit -f doit-chart/values-stage.yaml doit-chart
```

Or, if chart did not change, simply restart the deployment to re-pull latest image:

```shell
kubectl rollout restart -n doit-prod deployment doit-doit-chart 
```
