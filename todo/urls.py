from django.urls import path
from rest_framework.routers import SimpleRouter

from core.views.simple_model_viewset import gen_simple_model_view_set
from todo.models.todo_entry import TodoEntry
from todo.models.todo_group import TodoGroup
from todo.models.todo_list import TodoList
from todo.serializers.todo_entry import TodoEntrySerializer, TodoEntryCreateSerializer
from todo.serializers.todo_group import TodoGroupSerializer, TodoGroupCreateSerializer
from todo.serializers.todo_list import TodoListCreateSerializer, TodoListSerializer
from todo.views.todo_entry import get_entries_for_list
from todo.views.todo_list import get_lists_for_group

router = SimpleRouter()

router.register("groups", gen_simple_model_view_set(
    TodoGroup,
    "TODO group",
    TodoGroupSerializer,
    TodoGroupCreateSerializer,
))
router.register("lists", gen_simple_model_view_set(
    TodoList,
    "TODO list",
    TodoListSerializer,
    TodoListCreateSerializer,
))
router.register("entries", gen_simple_model_view_set(
    TodoEntry,
    "TODO entry",
    TodoEntrySerializer,
    TodoEntryCreateSerializer,
))

urlpatterns = [
    path('lists/for_group/<int:group_id>/', get_lists_for_group),
    path('entries/for_list/<int:list_id>/', get_entries_for_list),
]

urlpatterns += router.urls
