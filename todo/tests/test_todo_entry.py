from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from Organizer.tests.exceptions import RetrieveError, CreateError, DeleteError
from todo.tests.test_todo_list import create_todo_list


PRINT_RES = 'Result: '


def create_todo_entry(self: APITestCase) -> int:
    """
    Creates new instance of TodoEntry and returns its ID.
    May be used from other test cases to create dependencies.
    """
    data = {
        "list": create_todo_list(self),
        "name": "Test entry",
        "body": "Test entry body",
        "is_done": False,
    }
    # Force login as admin to bypass authentication checks
    self.client.force_login(User.objects.get(username='testuser'))
    response = self.client.post("/todo/entries/", data, format='json')
    if response.status_code != status.HTTP_201_CREATED:
        raise CreateError(PRINT_RES + str(response.data))
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    return response.data['id']


class TodoEntryTest(APITestCase):
    def test_create_todo_entry(self):
        """
        Creates new TodoEntry using POST request.
        """
        create_todo_entry(self)

    def test_retrieve_todo_entry(self):
        """
        Obtains first instance of TodoEntry using GET request.
        """
        # Force login as admin to bypass authentication checks
        self.client.force_login(User.objects.get(username='testuser'))
        obj_id = create_todo_entry(self)
        response = self.client.get(f"/todo/entries/{obj_id}/")
        if response.status_code != status.HTTP_200_OK:
            raise RetrieveError(PRINT_RES + str(response.data))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_todo_entry(self):
        """
        Deletes first instance of TodoEntry using DELETE request.
        """
        # Force login as admin to bypass authentication checks
        self.client.force_login(User.objects.get(username='testuser'))
        obj_id = create_todo_entry(self)
        response = self.client.delete(f"/todo/entries/{obj_id}/")
        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise DeleteError(PRINT_RES + str(response.data))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
