from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from Organizer.tests.exceptions import RetrieveError, CreateError, DeleteError


PRINT_RES = 'Result: '


def create_todo_group(self: APITestCase) -> int:
    """
    Creates new instance of TodoGroup and returns its ID.
    May be used from other test cases to create dependencies.
    """
    data = {
        'name': 'Test group'
    }
    # Force login as admin to bypass authentication checks
    self.client.force_login(User.objects.get(username='testuser'))
    response = self.client.post("/todo/groups/", data, format='json')
    if response.status_code != status.HTTP_201_CREATED:
        raise CreateError(PRINT_RES + str(response.data))
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    return response.data['id']


class TodoGroupTest(APITestCase):
    def test_create_todo_group(self):
        """
        Creates new TodoGroup using POST request.
        """
        create_todo_group(self)
        
    def test_retrieve_todo_group(self):
        """
        Obtains first instance of TodoGroup using GET request.
        """
        # Force login as admin to bypass authentication checks
        self.client.force_login(User.objects.get(username='testuser'))
        obj_id = create_todo_group(self)
        response = self.client.get(f"/todo/groups/{obj_id}/")
        if response.status_code != status.HTTP_200_OK:
            raise RetrieveError(PRINT_RES + str(response.data))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
    def test_delete_todo_group(self):
        """
        Deletes first instance of TodoGroup using DELETE request.
        """
        # Force login as admin to bypass authentication checks
        self.client.force_login(User.objects.get(username='testuser'))
        obj_id = create_todo_group(self)
        response = self.client.delete(f"/todo/groups/{obj_id}/")
        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise DeleteError(PRINT_RES + str(response.data))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
