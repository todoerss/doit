from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from Organizer.tests.exceptions import RetrieveError, CreateError, DeleteError
from todo.tests.test_todo_group import create_todo_group


PRINT_RES = 'Result: '


def create_todo_list(self: APITestCase) -> int:
    """
    Creates new instance of TodoList and returns its ID.
    May be used from other test cases to create dependencies.
    """
    data = {
        'group': create_todo_group(self),
        'name': 'Test list',
    }
    # Force login as admin to bypass authentication checks
    self.client.force_login(User.objects.get(username='testuser'))
    response = self.client.post("/todo/lists/", data, format='json')
    if response.status_code != status.HTTP_201_CREATED:
        raise CreateError(PRINT_RES + str(response.data))
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    return response.data['id']


class TodoListTest(APITestCase):
    def test_create_todo_list(self):
        """
        Creates new TodoList using POST request.
        """
        create_todo_list(self)

    def test_retrieve_todo_list(self):
        """
        Obtains first instance of TodoList using GET request.
        """
        # Force login as admin to bypass authentication checks
        self.client.force_login(User.objects.get(username='testuser'))
        obj_id = create_todo_list(self)
        response = self.client.get(f"/todo/lists/{obj_id}/")
        if response.status_code != status.HTTP_200_OK:
            raise RetrieveError(PRINT_RES + str(response.data))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_todo_list(self):
        """
        Deletes first instance of TodoList using DELETE request.
        """
        # Force login as admin to bypass authentication checks
        self.client.force_login(User.objects.get(username='testuser'))
        obj_id = create_todo_list(self)
        response = self.client.delete(f"/todo/lists/{obj_id}/")
        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise DeleteError(PRINT_RES + str(response.data))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
