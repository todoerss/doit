from rest_framework import serializers

from core.serializers.profile import ProfileSerializer
from todo.models.todo_entry import TodoEntry


class TodoEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoEntry
        fields = (
            "id",
            "list",
            "name",
            "body",
            "is_done",
            "deadline",
            "created_at",
            "updated_at",
            "created_by",
            "updated_by",
        )

    created_by = ProfileSerializer()
    updated_by = ProfileSerializer()


class TodoEntryCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoEntry
        fields = (
            "list",
            "name",
            "body",
            "is_done",
            "deadline",
        )
