from rest_framework import serializers

from core.serializers.profile import ProfileSerializer
from todo.models.todo_group import TodoGroup


class TodoGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoGroup
        fields = (
            "id",
            "name",
            "created_at",
            "updated_at",
            "created_by",
            "updated_by",
        )

    created_by = ProfileSerializer()
    updated_by = ProfileSerializer()


class TodoGroupCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoGroup
        fields = (
            "name",
        )
