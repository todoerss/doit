from rest_framework import serializers

from core.serializers.profile import ProfileSerializer
from todo.models.todo_list import TodoList


class TodoListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoList
        fields = (
            "id",
            "group",
            "name",
            "created_at",
            "updated_at",
            "created_by",
            "updated_by",
        )

    created_by = ProfileSerializer()
    updated_by = ProfileSerializer()


class TodoListCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoList
        fields = (
            "group",
            "name",
        )
