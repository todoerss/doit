# Generated by Django 4.0.1 on 2022-03-13 11:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial_models'),
        ('todo', '0002_add_todo_list_and_entry'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoentry',
            name='created_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='created_entries', to='core.profile', verbose_name='Creator'),
        ),
        migrations.AlterField(
            model_name='todoentry',
            name='updated_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='updated_entries', to='core.profile', verbose_name='Updater'),
        ),
        migrations.AlterField(
            model_name='todogroup',
            name='created_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='created_groups', to='core.profile', verbose_name='Creator'),
        ),
        migrations.AlterField(
            model_name='todogroup',
            name='updated_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='updated_groups', to='core.profile', verbose_name='Updater'),
        ),
        migrations.AlterField(
            model_name='todolist',
            name='created_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='created_lists', to='core.profile', verbose_name='Creator'),
        ),
        migrations.AlterField(
            model_name='todolist',
            name='updated_by',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='updated_lists', to='core.profile', verbose_name='Updater'),
        ),
    ]
