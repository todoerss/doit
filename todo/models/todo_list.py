import datetime

from crum import get_current_user
from django.db import models
from simple_history.models import HistoricalRecords

from Organizer.update_protocol.server_update import ServerUpdate, EventType, EntityName
from core.models.Profile import Profile
from todo.models.todo_group import TodoGroup


class TodoList(models.Model):
    class Meta:
        verbose_name = "TODO List"
        verbose_name_plural = "TODO Lists"

    group = models.ForeignKey(TodoGroup, verbose_name="Group", on_delete=models.CASCADE)

    name = models.TextField(verbose_name="Name")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        Profile,
        verbose_name="Creator",
        on_delete=models.PROTECT,
        blank=True,
        related_name='created_lists',
    )
    updated_by = models.ForeignKey(
        Profile,
        verbose_name="Updater",
        on_delete=models.PROTECT,
        blank=True,
        related_name='updated_lists',
    )
    history = HistoricalRecords()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, **kwargs):
        """
        Automatically fill creating and update info when saving the model.
        """
        is_update = True

        user = kwargs.get('user', None) or get_current_user()
        if user and not user.pk:
            user = None
        if not self.pk:
            self.created_by = user.profile.get()
            is_update = False
        self.updated_by = user.profile.get()
        result = super().save(force_insert, force_update, using, update_fields)

        # Local import to avoid circular import
        from todo.serializers.todo_list import TodoListSerializer
        ServerUpdate(
            date=datetime.datetime.now(),
            subsystem='todo',
            event_type=EventType.ENTITY_UPDATE if is_update else EventType.ENTITY_CREATE,
            entity_name=EntityName.TODO_LIST,
            entry=TodoListSerializer(self).data,
        ).send()

        return result

    def delete(self, using=None, keep_parents=False):
        # Local import to avoid circular import
        from todo.serializers.todo_list import TodoListSerializer
        ServerUpdate(
            date=datetime.datetime.now(),
            subsystem='todo',
            event_type=EventType.ENTITY_DELETE,
            entity_name=EntityName.TODO_LIST,
            entry=TodoListSerializer(self).data,
        ).send()

        return super().delete(using, keep_parents)
