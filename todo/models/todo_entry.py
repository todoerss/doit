import datetime

from crum import get_current_user
from django.db import models
from simple_history.models import HistoricalRecords

from Organizer.update_protocol.server_update import ServerUpdate, EventType, EntityName
from core.models.Profile import Profile
from todo.models.todo_list import TodoList


class TodoEntry(models.Model):
    class Meta:
        verbose_name = "TODO Entry"
        verbose_name_plural = "TODO Entries"

    list = models.ForeignKey(TodoList, verbose_name="List", on_delete=models.CASCADE)

    is_done = models.BooleanField(verbose_name="Is done", default=False, blank=True)
    name = models.TextField(verbose_name="Name")
    body = models.TextField(verbose_name="Entry body in Markdown", default="", blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        Profile,
        verbose_name="Creator",
        on_delete=models.PROTECT,
        blank=True,
        related_name='created_entries',
    )
    updated_by = models.ForeignKey(
        Profile,
        verbose_name="Updater",
        on_delete=models.PROTECT,
        blank=True,
        related_name='updated_entries',
    )
    history = HistoricalRecords()

    deadline = models.DateTimeField(verbose_name="deadline", blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, **kwargs):
        """
        Automatically fill creating and update info when saving the model.
        """
        is_update = True

        user = kwargs.get('user', None) or get_current_user()
        if user and not user.pk:
            user = None
        if not self.pk:
            self.created_by = user.profile.get()
            is_update = False
        self.updated_by = user.profile.get()
        result = super().save(force_insert, force_update, using, update_fields)

        # Local import to avoid circular import
        from todo.serializers.todo_entry import TodoEntrySerializer
        ServerUpdate(
            date=datetime.datetime.now(),
            subsystem='todo',
            event_type=EventType.ENTITY_UPDATE if is_update else EventType.ENTITY_CREATE,
            entity_name=EntityName.TODO_ENTRY,
            entry=TodoEntrySerializer(self).data,
        ).send()

        return result

    def delete(self, using=None, keep_parents=False):
        # Local import to avoid circular import
        from todo.serializers.todo_entry import TodoEntrySerializer
        ServerUpdate(
            date=datetime.datetime.now(),
            subsystem='todo',
            event_type=EventType.ENTITY_DELETE,
            entity_name=EntityName.TODO_ENTRY,
            entry=TodoEntrySerializer(self).data,
        ).send()

        return super().delete(using, keep_parents)
