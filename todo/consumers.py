from channels.generic.websocket import AsyncJsonWebsocketConsumer

from Organizer.update_protocol.server_update import ChannelGroup, ServerUpdate


class TodoConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.accept()
        print('client connected')
        await self.channel_layer.group_add(ChannelGroup.TODO.value, self.channel_name)

    async def todo_event(self, event):
        print('sending todo_event')
        server_update: ServerUpdate = event['server_update']
        await self.send_json(server_update.serialize())

    async def disconnect(self, code):
        await self.channel_layer.group_discard(ChannelGroup.TODO.value, self.channel_name)
