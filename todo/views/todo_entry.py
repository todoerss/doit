from drf_spectacular.utils import extend_schema
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from todo.models.todo_entry import TodoEntry
from todo.serializers.todo_entry import TodoEntrySerializer


@extend_schema(
    summary="Get list of TODO entries for a given list",
    request=None,
    responses={200: TodoEntrySerializer(many=True)}
)
@api_view()
def get_entries_for_list(request: Request, list_id: int):
    entries = TodoEntry.objects.filter(list=list_id).all()
    serializer = TodoEntrySerializer(entries, many=True)
    return Response(serializer.data)
