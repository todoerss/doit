from drf_spectacular.utils import extend_schema
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from todo.models.todo_list import TodoList
from todo.serializers.todo_list import TodoListSerializer


@extend_schema(
    summary="Get list of TODO lists for a given group",
    request=None,
    responses={200: TodoListSerializer(many=True)}
)
@api_view()
def get_lists_for_group(request: Request, group_id: int):
    lists = TodoList.objects.filter(group=group_id).all()
    serializer = TodoListSerializer(lists, many=True)
    return Response(serializer.data)
