# doit backend

Table of contents

1. [About the Project](#about)
    1. [Built and Tested With](#built-with)
2. [Getting Started](#getting-started)
    1. [Prerequisites](#prerequisites)
    2. [Installation](#installation)
3. [Usage](#usage)
4. [Testing](#testing)
5. [CI/CD](#ci)
7. [Contributing](#contributing)
8. [License](#license)
9. [Contact](#contact)
10. [Acknowledgements](#acknowledgements)

## About the Project <a name="about"></a>

doit is a universal task manager for all your needs: 
- organise your workflow;
- free your mind from duties;
- bring a balance to your life. 

### Built and Tested with <a name="built-with"></a>

- Python (Django framework)
- Django test framework (backend)

## Getting Started <a name="getting-started"></a>

In order to run the app locally, follow the next steps.

### Prerequisites <a name="prerequisites"></a>

You need python 3.9 installed.

### Preparing development environment <a name="installation"></a>

1. Clone repository
```bash
git clone https://gitlab.com/todoerss/doit.git
```

2. Go into its directory and execute:

```shell
python3 -m virtualenv venv
. venv/bin/activate
pip3 install -r requirements.txt
```

This will activate the virtual environment and install packages required for the app to run appropriately.

## Usage <a name="usage"></a>

From now on, you can start local server with

```shell
. venv/bin/activate
python3 manage.py migrate
DEBUG=True python3 manage.py runserver 0.0.0.0:8000
```

SQLite database is used by default, so you don't have to set up anything else. If you want to delete all data and start from scratch, delete file `db.sqlite3`.

Voi la!

## Testing <a name="testing"></a>

Backend includes tests written in DRF Test Framework. The tests cover creation/retrieving/deletion of backend entities, and execute the full pipeline of request dispatch. To test authentication, we utilize a dummy user with no password, so nobody can actually login as this user.

![Tests 1](./report-images/backend-tests1.png)

![Tests 2](./report-images/backend-tests2.png)

## CI/CD <a name="ci"></a>
We launched two types of servers. The first one, called "stage", is used to imitate production environment and test the functionality. The second one is used for production.

In CI/CD pipelines, there are stages for static code analysis, testing, building, pushing to DockerHub and deployment for each type of servers.

## Contributing <a name="contributing"></a>

Contributions are welcome! Teamwork and collaboration boost development of ideas and accelerate improvement of projects. If you would like to contribute to the project, follow the steps:

1. Fork the Project
2. Create a separate branch for your feature (```git checkout -b feature/YourFeature```)
3. Commit your Changes (```git commit -m 'Added some feature'```)
4. Push to the Branch (```git push origin feature/YourFeature```)
5. Open a Pull Request

For major modifications, open an issue to discuss what you want to change.

## Contacts <a name="contact"></a>

- Grigoriy Dolgov g.dolgov@innopolis.university
- Anna Gorb a.gorb@innopolis.university
- Marina Smirnova m.smirnova@innopolis.university
- Maxim Stepanov m.stepanov@innopolis.university