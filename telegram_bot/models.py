from django.db import models
from django.contrib.auth.models import User


class PairingRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="User")
    token = models.CharField(max_length=64, verbose_name="Token used to link user and chat_id")
