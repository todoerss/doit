from typing import List, Union

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.http import HttpResponse
from secrets import token_urlsafe
from django.views.decorators.csrf import csrf_exempt
import json

from Organizer import settings
from core.models.Profile import Profile
from todo.models.todo_entry import TodoEntry
from todo.models.todo_group import TodoGroup
from todo.models.todo_list import TodoList
from .models import PairingRequest
import requests


@login_required
def create(request):
    pairing_request, _created = request.user.pairingrequest_set.get_or_create(
        defaults={'token': token_urlsafe(8)}
    )

    bot_url = 'https://t.me/ToDoers_bot'
    return redirect(f'{bot_url}?start={pairing_request.token}')


def start(chat_id: int, text: List[str]):
    if len(text) == 0:
        send_message(chat_id, 'You failed too early. No token.')
        return

    token = text[0]

    try:
        req = PairingRequest.objects.get(token=token)
        # Update profile's Telegram Chat ID
        profile = req.user.profile
        profile.tg_chat_id = chat_id
        profile.save()

        content = "Welcome to DoIt!"
        send_message(chat_id, content)
    except PairingRequest.DoesNotExist:
        content = "Sorry, seems like you didn't start the bot"
        send_message(chat_id, content)


def send_message(chat_id: int, content: str):
    send_message_url = f'https://api.telegram.org/bot{settings.bot_key}/sendMessage?chat_id={chat_id}&text={content}'
    requests.post(send_message_url)


def create_entry(chat_id: int, text: List[str]):
    try:
        profile = Profile.objects.get(tg_chat_id=chat_id)
        group = TodoGroup.objects.get_or_create(name="Inbox", defaults={
            'user': profile.user,
            'name': "Inbox",
        })
        todo_list = TodoList.objects.get_or_create(group=group, name="Telegram", defaults={
            'user': profile.user,
            'group': group,
            'name': "Telegram",
        })
        TodoEntry.objects.create(list=todo_list, name=' '.join(text), body='', user=profile.user)
        send_message(chat_id, 'Entry created!')
    except Profile.DoesNotExist:
        send_message(chat_id, 'You failed')


mapping = {
    '/start': start,
    '/create_entry': create_entry,
}


@csrf_exempt
def callback(request):
    # telegram calls the server

    body = json.loads(request.body)
    print(body)
    text: List[str] = body['message']['text'].split(' ')  # the text field looks like "text": "/start uEDbtJFHxKc"

    if len(text) > 0 and text[0].startswith('/'):
        f = mapping.get(text[0], None)
        if f is not None:
            f(
                body['message']['chat']['id'],
                text[1:]
            )

    return HttpResponse()
