from django.urls import path
from . import views

urlpatterns = [
    path('pair', views.create, name='pair'),
    path('webhook', views.callback, name='webhook'),
]