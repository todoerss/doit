from typing import Type

from django.db.models import QuerySet, Model
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.exceptions import NotFound
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, \
    DestroyModelMixin
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import GenericViewSet


def gen_simple_model_view_set(model: Type[Model],
                              model_name: str,
                              retrieve_serializer: Type[ModelSerializer],
                              create_serializer: Type[ModelSerializer],
                              update_serializer: Type[ModelSerializer] = None,
                              **kwargs):
    """
    Base ViewSet for container-state models. Automatically manages the update of state actuality dates when updating
    model.
    """

    _model: Type[Model]
    _model_name: str
    _retrieve_serializer: Type[ModelSerializer]
    _create_serializer: Type[ModelSerializer]
    _update_serializer: Type[ModelSerializer]

    if model is None:
        raise TypeError("Model passed to SimpleModelViewSet is None")
    if model_name is None:
        raise TypeError("Model name passed to SimpleModelViewSet is None")
    if retrieve_serializer is None:
        raise TypeError("Retrieve serializer passed to SimpleModelViewSet is None")
    if create_serializer is None:
        raise TypeError("Create serializer passed to SimpleModelViewSet is None")

    _model = model
    _model_name = model_name
    _retrieve_serializer = retrieve_serializer
    _create_serializer = create_serializer
    _update_serializer = update_serializer if update_serializer is not None else create_serializer

    class SimpleModelViewSet(GenericViewSet,
                             ListModelMixin,
                             CreateModelMixin,
                             RetrieveModelMixin,
                             UpdateModelMixin,
                             DestroyModelMixin):
        queryset = _model.objects.all()
        serializer_class = _retrieve_serializer

        def get_object(self):
            try:
                obj = _model.objects.filter(pk=self.kwargs[self.lookup_field]).get()
                return obj
            except _model.DoesNotExist:
                raise NotFound(f"Instance of {_model.__name__} with id={self.kwargs[self.lookup_field]} does not exist")

        def get_queryset(self) -> QuerySet:
            return _model.objects.all()

        @extend_schema(
            summary=f"Get list of {_model_name}",
            responses={
                200: _retrieve_serializer(many=True),
            },
        )
        def list(self, request, *args, **kwargs):
            return super().list(request, *args, **kwargs)

        @extend_schema(
            summary=f"Get instance of {_model_name}",
            responses={
                200: _retrieve_serializer(many=True),
            },
        )
        def retrieve(self, request, *args, **kwargs):
            return super().retrieve(request, *args, **kwargs)

        @extend_schema(
            summary=f"Create {_model_name}",
            request=_create_serializer,
            responses={
                201: _retrieve_serializer()
            },
        )
        def create(self, request, *args, **kwargs):
            serializer = _create_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            instance = serializer.save()
            response = _retrieve_serializer(instance, context=self.get_serializer_context()).data
            headers = self.get_success_headers(response)
            return Response(response, status=status.HTTP_201_CREATED, headers=headers)

        @extend_schema(
            summary=f"Update {_model_name}",
            request=_update_serializer,
            responses={200: _retrieve_serializer()},
        )
        def update(self, request, *args, **kwargs):
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = _update_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            instance = serializer.save()
            response = _retrieve_serializer(instance, context=self.get_serializer_context()).data
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}
            return Response(response)

        @extend_schema(
            summary=f"Patch {_model_name}",
            request=_update_serializer,
            responses={
                200: _retrieve_serializer()
            },
        )
        def partial_update(self, request, *args, **kwargs):
            # Partial update calls update internally, so there's no need to manually redefine the logic
            return super().partial_update(request, *args, **kwargs)

        @extend_schema(
            summary=f"Delete {_model_name}",
            responses={
                204: ''
            },
        )
        def destroy(self, request, *args, **kwargs):
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)

    return SimpleModelViewSet
