# Generated by Django 4.0.1 on 2022-03-27 14:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_create_testuser_for_tests'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='tg_chat_id',
            field=models.IntegerField(default=None, verbose_name='Telegram Bot Chat ID', null=True),
        ),
    ]
