# Generated by Django 4.0.1 on 2022-03-15 06:51

from django.db import migrations


def create(apps, schema):
    User = apps.get_model('auth.User')
    Profile = apps.get_model('core.Profile')

    user = User(
        username='testuser',
        first_name="test",
        last_name='user',
        email='example@example.com',
    )
    user.save()
    profile = Profile(
        user=user
    )
    profile.save()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0002_add_profile_related_name'),
    ]

    operations = [
        migrations.RunPython(
            code=create,
            reverse_code=migrations.RunPython.noop,
        )
    ]
