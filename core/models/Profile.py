from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.ForeignKey(User, primary_key=True, on_delete=models.CASCADE, related_name='profile')
    tg_chat_id = models.IntegerField(verbose_name="Telegram Bot Chat ID", default=None, null=True)
    enable_tg_notifications = models.BooleanField(default=True)


@receiver(post_save, sender=User)
def create_user_profile(sender: User, instance: User, created: bool, **kwargs) -> None:
    """
    Signal receiver to automatically create user Profile when User is created.
    """
    if created:
        Profile.objects.create(user=instance)
