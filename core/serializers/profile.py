from django.core.validators import validate_email, EmailValidator
from rest_framework import serializers

from core.models.Profile import Profile


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
        )

    id = serializers.ReadOnlyField(source="user.id")
    first_name = serializers.ReadOnlyField(source="user.first_name")
    last_name = serializers.ReadOnlyField(source="user.last_name")
    email = serializers.ReadOnlyField(source="user.email")


class ProfileUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            "first_name",
            "last_name",
            "email",
        )

    first_name = serializers.CharField(min_length=0, max_length=1024)
    last_name = serializers.CharField(min_length=0, max_length=1024)
    email = serializers.CharField(min_length=0, max_length=1024, validators=[EmailValidator])

    def update(self, instance: Profile, validated_data):
        user = instance.user

        user.first_name = validated_data['first_name']
        user.last_name = validated_data['last_name']
        user.email = validated_data['email']
        user.save()

        return instance
