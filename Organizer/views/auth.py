from drf_spectacular.utils import extend_schema
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from Organizer.serializers.auth import LoginSerializer, LoginResponseSerializer, RegisterSerializer
from core.models.Profile import Profile
from core.serializers.profile import ProfileSerializer, ProfileUpdateSerializer


class LoginAPIView(GenericAPIView):
    permission_classes = ()
    serializer_class = LoginSerializer

    @extend_schema(
        summary="Login with given credentials",
        request=LoginSerializer,
        responses={
            200: LoginResponseSerializer,
        },
    )
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data)


class RegisterAPIView(GenericAPIView):
    permission_classes = ()
    serializer_class = RegisterSerializer

    @extend_schema(
        summary="Register with given credentials",
        request=RegisterSerializer,
        responses={
            200: LoginResponseSerializer,
        },
    )
    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data)


class CurrentProfileAPIView(GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = ProfileSerializer

    @extend_schema(
        summary="Get current user profile",
        request=None,
        responses={200: ProfileSerializer},
    )
    def get(self, request):
        serializer = ProfileSerializer(Profile.objects.get(user=request.user))
        return Response(serializer.data)

    @extend_schema(
        summary="Update current user profile",
        request=ProfileUpdateSerializer,
        responses={200: ProfileSerializer},
    )
    def put(self, request):
        # Update database with new values
        serializer = ProfileUpdateSerializer(instance=Profile.objects.get(user=request.user), data=request.data)
        serializer.is_valid()
        serializer.save()

        # Respond to user with new data
        response_serializer = ProfileSerializer(Profile.objects.get(user=request.user))
        return Response(response_serializer.data)
