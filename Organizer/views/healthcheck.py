from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.request import Request


@csrf_exempt
def healthcheck(request: Request):
    return HttpResponse("I'm alive", status=status.HTTP_200_OK)
