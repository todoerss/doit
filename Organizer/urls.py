from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView, SpectacularRedocView

from Organizer import settings
from Organizer.views.healthcheck import healthcheck
from Organizer.views.auth import LoginAPIView, RegisterAPIView, CurrentProfileAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('healthcheck/', healthcheck),
    path("login/", LoginAPIView.as_view()),
    path("register/", RegisterAPIView.as_view()),
    path("current_profile/", CurrentProfileAPIView.as_view()),

    # OpenAPI schema
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    # API UI
    path('api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('api/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),

    path("todo/", include("todo.urls")),
    path("tg-bot/", include("telegram_bot.urls")),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
