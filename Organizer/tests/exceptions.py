class CreateError(Exception):
    """
    Used in test cases to indicate error during object creation.
    """
    pass


class RetrieveError(Exception):
    """
    Used in test cases to indicate error during object retrieval.
    """
    pass


class UpdateError(Exception):
    """
    Used in test cases to indicate error during object update.
    """
    pass


class DeleteError(Exception):
    """
    Used in test cases to indicate error during object deletion.
    """
    pass
