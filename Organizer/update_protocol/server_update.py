import datetime
import enum
import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


class ChannelGroup(enum.Enum):
    TODO = "todo"


class EventType(enum.Enum):
    ENTITY_CREATE = "entity.create"
    ENTITY_UPDATE = "entity.update"
    ENTITY_DELETE = "entity.delete"
    ENTITY_REORDER = "entity.reorder"


class EntityName(enum.Enum):
    TODO_ENTRY = "todo_entry"
    TODO_LIST = "todo_list"
    TODO_GROUP = "todo_group"


class ServerUpdate():
    date: datetime.datetime
    subsystem: str
    event_type: EventType
    entity_name: EntityName
    entry: str

    def __init__(self,
                 date: datetime.datetime,
                 subsystem: str,
                 event_type: EventType,
                 entity_name: EntityName,
                 entry: str
                 ):
        self.date = date
        self.subsystem = subsystem
        self.event_type = event_type
        self.entity_name = entity_name
        self.entry = entry

    def serialize(self) -> dict:
        return {
            'datetime': self.date.isoformat(),
            'subsystem': self.subsystem,
            'event_type': self.event_type.value,
            'entity_name': self.entity_name.value,
            'entry': self.entry,
        }

    def send(self):
        print('sending server_update')
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(ChannelGroup.TODO.value, {
            # Type resolves to method name in the consumer
            'type': 'todo_event',
            'server_update': self,
        })
