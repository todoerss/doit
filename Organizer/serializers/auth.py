from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

from core.serializers.profile import ProfileSerializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
        )


class LoginResponseSerializer(serializers.Serializer):
    token = serializers.SerializerMethodField(help_text="API token")
    profile = serializers.SerializerMethodField()

    @extend_schema_field(serializers.CharField())
    def get_token(self, user):
        token, created = Token.objects.get_or_create(user=user)
        return token.key

    @extend_schema_field(UserSerializer)
    def get_profile(self, user):
        return ProfileSerializer(user.profile, context=self.context).data

    def create(self, validated_data):
        raise NotImplementedError

    def update(self, instance, validated_data):
        raise NotImplementedError


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    def validate(self, attrs):
        user = get_user_model().objects.filter(username=attrs["username"]).first()
        if not user:
            raise serializers.ValidationError({"username": f"User with login {attrs['username']} does not exist"})
        return attrs

    def create(self, validated_data):
        username = validated_data["username"]
        password = validated_data["password"]
        user = get_user_model().objects.get(username=username)
        if not user.check_password(password):
            raise serializers.ValidationError({"password": "Incorrect password"})

        return LoginResponseSerializer(instance=user, context=self.context).data

    def update(self, instance, validated_data):
        raise NotImplementedError


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    password = serializers.CharField(required=True, validators=[validate_password])
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=get_user_model().objects.all())])

    def validate(self, attrs):
        user = get_user_model().objects.filter(username=attrs["username"]).first()
        # Abort registration if user with such username already exists
        if user:
            raise serializers.ValidationError({"username": f"User with login {attrs['username']} already exists"})
        return attrs

    def create(self, validated_data):
        username = validated_data["username"]
        password = validated_data["password"]
        first_name = validated_data["first_name"]
        last_name = validated_data["last_name"]
        email = validated_data["email"]

        # Support creation of admin user
        is_admin = username == 'admin'

        user = get_user_model().objects.create(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name,
            # Both fields are required to log in to Django Admin app
            is_superuser=is_admin,
            is_staff=is_admin,
        )
        # Update password (it is hashed internally by Django)
        user.set_password(password)
        user.save()

        return LoginResponseSerializer(instance=user, context=self.context).data

    def update(self, instance, validated_data):
        raise NotImplementedError
